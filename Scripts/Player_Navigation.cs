﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Navigation : MonoBehaviour
{
    public float moveTime;
    float invMoveTime;

    public LayerMask blockingLayer;

    Rigidbody rb;
    Collider coll;

    void Start()
    {
        invMoveTime = 1f / moveTime;
        rb = GetComponent<Rigidbody>();
        coll = GetComponent<Collider>();
    }

    bool Move(int xDir, int zDir, out RaycastHit hit)
    {
        Vector3 start = transform.position;
        Vector3 end = transform.position + new Vector3(xDir, 0, zDir);

        Physics.Linecast(start, end, out hit, blockingLayer);
        if(hit.transform == null) {
            //StartCoroutine
            return true;
        }
        else {
            return false;
        }
    }

    IEnumerator SmoothMovement(Vector3 end)
    {
        float sqrRemainingDist = (transform.position - end).sqrMagnitude;

        while (sqrRemainingDist > float.Epsilon)
        {
            Vector3 newPosition = Vector3.MoveTowards(rb.position, end, invMoveTime * Time.deltaTime);
            rb.MovePosition(newPosition);
            sqrRemainingDist = (transform.position - end).sqrMagnitude;
            yield return null;
        }
    }

    void AttemptMove(int xDir, int zDir)
    {
        RaycastHit hit;
        bool canMove = Move(xDir, zDir, out hit);
    }
}
