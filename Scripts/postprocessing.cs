﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class postprocessing : MonoBehaviour
{
    [ExecuteInEditMode]

    public Material post;

    [SerializeField, Range(0, 1)]
    float _intensity = 0;
    public float intensity
    {
        get { return _intensity; }
        set { _intensity = value; }
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        post.SetFloat("_Intensity", _intensity);

        Graphics.Blit(source, destination, post);
    }
}
