﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Animator))]
public class PlayerMovement : Movement
{
    Animator anim;

    public int moveDistance = 3;//Set to Length of tile

    //Debug Variables

    protected override void Start()
    {
        anim = GetComponent<Animator>();

        base.Start();
    }

    private void FixedUpdate()
    {
        int horizontal = 0;
        int vertical = 0;

        horizontal = (int)Input.GetAxisRaw("Horizontal");
        vertical = (int)Input.GetAxisRaw("Vertical");

        if(horizontal != 0)
        {
            vertical = 0;
        }

        if(horizontal != 0 || vertical != 0)
        {
            if (canMove)
            {
                AttemptMove(horizontal * moveDistance, vertical * moveDistance); //Vertical is zAxis
            }
        }
    }

    protected override void AttemptMove (int xDir, int zDir)
    {
        base.AttemptMove(xDir, zDir);

        RaycastHit hit;
        if(Move(xDir, zDir, out hit))
        {
            //Play SFX
        }

        //Add Gamemanager to handle true/false for player movement
    }

    private void OnTriggerEnter(Collider other) //use to enable item pickup & engage in combat
    {
        if(other.tag == "Enemy")
        {
            Debug.Log("Trigger: Enemy");
        }
        else if(other.tag == "Item")
        {
            Debug.Log("Trigger: Item");

            other.gameObject.SetActive(false);
        }else if(other.tag == "Exit")
        {
            Debug.Log("Trigger: Exit");

            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);
        }
    }
}
